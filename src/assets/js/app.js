const svgContainer = document.getElementById('container');

bodymovin.loadAnimation({
	wrapper: svgContainer,
	animType: 'svg',
	loop: true,
	path: '/assets/js/animation.json'
});