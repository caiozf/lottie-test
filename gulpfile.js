let gulp = require('gulp'),
 	clean = require('gulp-clean'),
 	cssnano = require('gulp-cssnano'),
 	autoprefixer = require('gulp-autoprefixer'),
 	svgSprite = require('gulp-svg-sprite'),
 	sass = require('gulp-sass'),
 	plumber = require('gulp-plumber'),
  browserSync = require('browser-sync'),
  htmlnano = require('gulp-htmlnano'),
  options = {
      removeComments: false
  };

sass.compiler = require('node-sass')

let config = {
	shape: {
      spacing: {
        padding: 10
      }
    },
    mode: {
      css: { 
        render: {
          scss: {
          	dest: '/sass/'
          } ,
          dest: '',
          sprite: '/img/'
        }
      }
    }
};

gulp.task('build', ['clean','build-sass', 'copy', 'build-html']);

gulp.task('copy', ['clean'], function() {

  return gulp
    .src('./src/**/*')
    .pipe(gulp.dest('dist'));
});

gulp.task('build-html', ['copy'] , function() {
  return gulp
      .src('./src/index.html')
      .pipe(htmlnano(options))
      .pipe(gulp.dest('./dist'));
});

gulp.task('clean', function() {
  return gulp.src('dist')
    .pipe(clean());
});

gulp.task('server', ['build-sass', 'browserSync'], function() {

  gulp.watch('src/assets/sass/*.scss').on('change', function (){
    
    gulp.start('build-sass');
  });
  gulp.watch('src/**/*').on('change', browserSync.reload);
});

gulp.task('browserSync', function() {

  browserSync.init({
     server: {
       baseDir: './src/'
     }
  });
});

gulp.task('build-sass', function(){
  return gulp.src('./src/assets/sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
        browsers: ['last 10 versions'],
        cascade: false
    }))
    .pipe(cssnano({zindex: false}))
    .pipe(gulp.dest('./src/assets/css/'))
    .pipe(browserSync.stream());
});

 
gulp.task('sprite', function(){
	gulp.src('src/assets/img/icons/*.svg', { cwd: '' })
	  .pipe(plumber())
	  .pipe(svgSprite(config))
	  .pipe(gulp.dest('./src/assets/'));
});
